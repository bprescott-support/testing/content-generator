# Self generating project

## what's it based on?

This is a self generating large test project for exercising project import and export.

It's based on:

- https://gitlab.com/gitlab-org/quality/performance-data
  - see [documentation for the data generator](docs/data_generator.md)

## purpose

To generate a project with some activity and records, and pipelines.

Pipeline and job creation requires you to wait for lots of pipelines to schedule and run
(modify the supplied CI to create more jobs, more child pipelines, etc).

For issues, MRs, etc, the data generation script is API based. This could probably
generate a vast amount of content given the limitations of job timeouts, not least
the hard coded timeout set on the job in the CI.

## pre-requisites

### docker runner

- a runner capable of running jobs in a container is required for the data generation
job.  One of the distribution team's containers us specified to avoid the issue of
docker rate limiting.

### rate limit

- [default is 300/minute](https://docs.gitlab.com/ee/user/admin_area/settings/rate_limit_on_notes_creation.html)
- Admin area > Settings > Network > Notes Rate Limits
- I set:
  - `Max requests per minute per user` to 3000
  - `List of users to be excluded from the limit` to the bot userID.
- Probably only one of these is needed

### project access token

- required to generate issues, merge requests, branches, comments, and labels
- Settings > Access Tokens
- Scopes: `read_repository`, `write_repository`, `api` works
- Creates a bot account, who'll be the owning account on everything.

### sidekiq cron

- if you want to run scheduled pipelines as fast as possible, your instances `gitlab.rb` needs modifying
to persuade sidekiq to run CI schedules every minute. Otherwise, you'll be wondering why take so long to
get started

```
gitlab_rails['pipeline_schedule_worker_cron'] = "* * * * *"
```

## how to use

### branches

The repository has a `main` branch and a `master branch`, each do different things
like 'Jekyll' and 'Hyde'.

The default branch should be set to `main`, this is where the activity generation, CI code,
README etc. are.

The `master` branch is used by the activity generator script for creating branches, and if
you test merge requests, set `master` as the target.

### nothing happens!

When you push `master` and `main`, `envdump` will run. This job will always run
and exists to allow you to debug why other things aren't happening.

Jobs use `CI_PIPELINE_SOURCE` to control when they run.

If you push `main` or branches based on it, raise MRs and so on, `CI_PIPELINE_SOURCE` is
set to `push`, `merge_request_event` and so on, and the jobs don't trigger.

### generate pipelines and jobs

- jobs in `ci/jobsrus.yml` and `ci/child.yml` run
- purpose is to generate runner load, create pipeline and job database entries
- a shell runner is enough for these jobs

#### adhoc

- start a pipeline for the `main` branch, or a branch based on it
  - this will set `CI_PIPELINE_SOURCE` to `web`
- set CI variable `PIPELINE_ACTION` to `generate-jobs`

#### schedule

- this will set `CI_PIPELINE_SOURCE` to `schedule`
- set CI variable `PIPELINE_ACTION` to `generate-jobs`

### generate issues, merge requests, branches, comments, and labels

- jobs in `ci/datagen.yml` run
- runs in a container, requires runner(s) capable of doing this.
- GitLab objects are created in the same project as the job is running
- if a source branch is needed (merge requests, branches) `master` is used

#### adhoc

- start a pipeline for the `main` branch, or a branch based on it
  - this will set `CI_PIPELINE_SOURCE` to `web`
- set CI variable `PIPELINE_ACTION` to `generate-data`
- set CI variable `ACCESS_TOKEN` with an access token

#### schedule

- this will set `CI_PIPELINE_SOURCE` to `schedule`
- start a pipeline for the `main` branch, or a branch based on it
- set CI variable `PIPELINE_ACTION` to `generate-data`
- set CI variable `ACCESS_TOKEN` with an access token
